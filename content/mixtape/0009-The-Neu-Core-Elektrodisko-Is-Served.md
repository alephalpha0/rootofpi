---
title: "[0009] The Nue Core Elektrodisko Is Served."
date: 2022-05-02T23:21:38Z
draft: false
categories: ["mixtape"]
tags: ["mixtape"]
summary: "Grind and groove and let the elektro take you over."
---

## [0009] __THE NEU CORE ELEKTRODISKO IS SERVED__

* Tepr - Tale of Devotion (Prins Thomas Diskomiks) __[Tale of Devotion]__
* KLLO - Virtue __[Backwater]__
* M83 - Do It, Try It (TEPR Remix) __[Do It, Try It Remixes]__
* The Juan Maclean - Running Back To You __[In A Dream]__
* Goldfrapp - Anymore __[Silver Eye]__
* The Hics - Tangle __[Tangle EP]__
* Tepr - Taste of Love (feat. D. Woods) (Madeaux Remix)
* Goldfrapp - Slide In (DFA Remix) __[Ride a White Horse]__
* Athlete Whippet & Olugbenga - All at Once
