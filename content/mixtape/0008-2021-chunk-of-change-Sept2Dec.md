---
title: "[0008] 2021 Chunk of Change Sept2Dec"
date: 2022-03-19T15:27:05Z
draft: false
categories: ["mixtape"]
summary: "The 2021 September to December installment of my Chunk of Change series."
---

## A Chunk of Change is my series of Mixtapes cobbled together over a couple months from the gems of aural pleasure Spotify throws my way. There is no central theme, there is no cohesion, there is no real point to the series. It's sole purpose is to keep track of song's I put on repeat and never looked back from.

### Here is the volume from September to December of the year 2021.

* Hood Rich - Bring A Friend 
* deadmau5 & Lights - When The Summer Dies
* !!! - Sunday 5:17 a.m. (Justin B. Remix)
* Fake Blood - I Think I Like It __[Fix Your Accent]__
* Daft Punk & Panda Bear - Doin' it Right __[Random Access Memories]__
* The Drifters - Some Kind of Wonderful
* Belaganas - Ford vs Ferrari
* Luke Levenson & Abbey Smith - Smoke
* Miley Cyrus - Zombie (Live from the NIVA Save Our Stages Festival)
* Steely Dan - Peg __[Aja]__
* Willie Nelson - Just Dropped In (To See What Condition My Condition Was In) __[The Rainbow Connection]
* Chromatics - Lady
* Shygirl - Cleo
* Party Favor - Losing My Mind (with Elohim)
* Skrillex - Supersonic (My Existence) [with Noisia, josh pam & Dylan Brady]
* Grace Jones - Pull Up To The Bumper
* Lucky Luke - Cooler Than Me
