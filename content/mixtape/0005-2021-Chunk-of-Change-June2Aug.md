---
title: "[0005] 2021 A Chunk of Change - June2Aug"
date: 2021-10-30T15:22:17Z
draft: false
categories: ["mixtape"]
tags: ["mixtape"]
summary: "MAKE SURE THIS ISNT CLEAR AT FUCKING ALL. JUST DO WHAT YOU WANT BUT MAKE IT FIT THE FUCKING THEME!"
---

## A Chunk of Change - June2Aug 2021

* Romare - Down the Line - It Takes A Number __[Meditations on Afrocentrism]__ 
* PVA - Talks (Mura Masa Remix) __[Talks (Remixes)]__
* Dick Dale - Surfing Drums __[Guitar Legend: The Very Best of Dick Dale]__
* The Hics - The Man Who Sold the World __[The Man Who Sold the World Single]__

### A Chunk of Change is my series of Mixtapes cobbled together over a couple months from the gems of aural pleasure Spotify throws my way. There is no central theme, there is no cohesion, there is no real point to the series. It's sole purpose is to keep track of song's I put on repeat and never looked back from.
