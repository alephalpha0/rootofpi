---
title: "[0010] Payable Upon Delivery."
date: 2022-11-12T17:26:38Z
draft: false
categories: ["mixtape"]
tags: ["mixtape", "we are the music makers"]
summary: "Driving away from his house with the evidence secured in my glovebox."
---

## [0010] __PAYABLE UPON DELIVERY__

* Kali Uchis - Loner __[Loner EP]__
* Tony Castles -- Heart in the Pipes (KAUF remix)
* Sharon Jones & The Dap-Kings - 100 Days, 100 Nights __[100 Days, 100 Nights]__
* Jessie Reyez - IMPORTED (with 6LACK) __[BEFORE LOVE CAME TO KILL US]__
* Kllo - Dissolve __[Backwater]__
* Teeth of the Sea - Field Punishment __[Highly Deadly Black Tarantula]__
