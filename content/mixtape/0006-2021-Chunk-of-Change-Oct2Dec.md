---
title: "[0006] 2021 A Chunk of Change - October to December"
date: 2021-12-07T06:22:24Z
draft: false
categories: ["mixtape"]
tags: ["mixtape", "pop rocks"]
summary: "The 2021 October to December mini-mix installment of my Chunk of Change series."
---

## A Chunk of Change - Oct2Dec 2021

* Golden Features - Raka  __[Raka]__
* Lusine - Not Alone (ft. Jenn Champion) __[Retrace]__
* Peaches - Close Up (ft. Kim Gordon) __[Rub]__
* !!! - Dancing Is The Best Revenge __[Shake The Shudder]__
* Mindchatter - It's Been You __[Imaginary Audience]__
* Yaeji - 29 __[Year to Year / 29]__

### A Chunk of Change is my series of Mixtapes cobbled together over a couple months from the gems of aural pleasure Spotify throws my way. There is no central theme, there is no cohesion, there is no real point to the series. It's sole purpose is to keep track of song's I put on repeat and never looked back from.
