---
title: "[0003] 2021 A Chunk of Change - Aug2Oct"
date: 2021-10-14T01:57:42Z
draft: true
categories: ["mixtape"]
tags: [""]
summary: "The 2021 August to October installment of my Chunk of Change series."
---

## A Chunk of Change is my series of Mixtapes cobbled together over a couple months from the gems of aural pleasure Spotify throws my way. There is no central theme, there is no cohesion, there is no real point to the series. It's sole purpose is to keep track of song's I put on repeat and never looked back from.

### Here is the volume from August to October of the year 2021.

* MEMBA - Sun Sala __[Sun Sala EP]__
* Amaarea - SAD GIRLZ LUV MONEY (W/ Moliy) __[THE ANGEL YOU DON'T KNOW]__
