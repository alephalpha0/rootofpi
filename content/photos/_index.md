**Since middle school I have owned**, in one form or another, a digital camera. My archive of photographs extends from the early 2000s to current day 2023. Digital scans, canon powershots, lg freebie phones and beyond have been my medium for over two decades.

There is something deeply emotionally satisfying of coming across a place in space and time that tells me how it wants to be framed and captured. It feels closer to divine when I get it right.
