---
title: "[Photograph] A Path off Trinity"
date: 2023-09-05T16:07:13Z
categories: ["photographs"]
tags: ["photographs", "sub-liminal spaces"]
location: "Gateway park portion of the Trinity River."
---

![Path off Trinity.](/img/photos/pathOffTrinity.jpg)

#### ~~2019-XX-XX~~ 
