---
title: "2022 04 09 Ssh Permissions"
date: 2022-04-09T10:57:13Z
draft: false
categories: ["tech"]
summary: "~/.ssh and it's various files have specific permissions. Hopefully, I won\'t forget them again."
---

# [OpenSSH](https://openssh.com) - Keeping Your Communiques Secret

## New Server, Who Dis?
> Everytime I spin up a new server or install another distro, I always come back to staring blankly at the command prompt.. cursing myself for not writing down the simple octal permissions needed for OpenSSH client & server to just **work.**

```
chmod 700 ~/.ssh
chmod 644 ~/.ssh/*.pub
touch ~/.ssh/authorized_keys ~/.ssh/known_hosts ~/.ssh/config
chmod 644 ~/.ssh/authorized_keys ~/.ssh/known_hosts ~/.ssh/config
chmod 600 ~/.ssh/ALL_PRIVATE_KEYS
```

## `700 for .ssh/`

## `600 for private keys.`

## `644 for e'eryting else.`
