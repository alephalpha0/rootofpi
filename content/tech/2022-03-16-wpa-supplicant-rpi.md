---
title: "WPA_Supplicant.conf and the Raspberry Pi."
date: 2022-03-16T06:00:23Z
draft: false
categories: ["tech"]
tags: ["raspberry pi", "geek me good"]
summary: "Setting up a headless Raspberry Pi requires the wpa_supplicant.conf to be pre-configured."
---

I've been messing around with my Raspberry Pi Zero W for a little over a year now.. With a near-monthly schedule ofthrowing a new OS on a microSD card and initializing the first boot, setting up some repos, tinkering a little in the configs.. Always headless inits and SSH forrays (I honestly don't think I have once used the HDMI port on my `creampi`), requiring the placement of `wpa_supplicant.conf` in the root directory and a customary `touch ssh` to enable the SSHD.

And to this day, I cannot fucking remember the easy-peasy syntax of `wpa_supplicant.conf` and I have yet to keep a copy of it around to copy into la OS de jour..

THIS ENDS NOW!

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
country=US
update_config=1

network={
ssid="GIVE ME INTERNET I CRAVE IT"
scan_ssid=1
psk="POOP POOP HAHA SQUISHY HEAD"
key_mgmt=WPA2-PSK
}
```
