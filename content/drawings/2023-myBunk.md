---
title: "The Bunk Where I Sleep."
date: 2023-05-06T17:36:31Z
categories: ["drawings", "blog"]
---

**My living situation** has improved since being evicted last month. I've gone from sleeping (intermittently) on Lisa's couch to sleeping (more than I need) in a bunk bed at a hostel-house here in Fort Worth. I have yet to sleep outside. *knock on wood* 

It's a different head space to not have a "place" to call my own. I'm adjusting slowly to it, to the best of my ability. Also coming to the realization that the concept of personal property is ephemeral at best and ludicrous at worst. I have "lost" everything that I could call "mine" twice in the past 5ish years. I use quotes because I didn't lose my property by force of a third party, rather through laziness and a general lack of concern for things like rent and paying consistently on time. You know, the general basics of capitalism. 

Slowly, I'm learning to adult willingly. I am pulling in a good 38 hours a week at work. I paid for the bed rental a week in advance and have saved up enough money in various accounts to cover any minor emergency that may arise. Mental health appointments went by the way-side last week due to my over-attachment to sleep.. To counter that, I finished my eligibility with MHMR up do I'm all good for financial assistance and appointments going forward. Reinstatement with Healing Wings and JPS are also on the books for this coming week.. 

![A Diagram of my bunk](/img/drawings/myBunk.png)
