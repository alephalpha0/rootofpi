---
title: "The Routine"
date: 2023-05-17T23:50:44Z
categories: ["blog"]
---

I am getting used to the routine. It goes a little something like this:
* Days I work:
  * Try to wake up around 7am to get ready to go eat breakfast at Samaritan House.
  * Catch a zipzone to SH and eat breakfast and catch a zipzone back to the hostel.
  * Play around on my phone, either a round of Wild Rift or Dead Cells or Legends of Runeterra for a couple hours.
  * Take a nap until it's time to get a zipzone back to workc
  * Walk to the pickup and hope that I get Alfred or Ruth as a driver this time.
  * Usually, I woke up late so I get to work a little late.
  * Work and work and work. Time goes by relatively quickly still. 10pm never comes fast enough tho.
  * Catch the #1 down Hemphill to Berry and walk the rest of the way back to the hostel.
  * Climb into my bunk bed and strip to cool down. 
  * Play a round of Wild Rift until I fall asleepc
 
* Days I dont work:
  * Either:
    * Adult or
    * Sleep all damn day long.

