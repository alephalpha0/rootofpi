---
title: "info description for false"
date: 2021-12-22T18:07:46Z
tags: ["technology", "command line gazing", "geek me good", "stream of consciousness", "linux fluff"]
categories: ["stream of consciousness"]
---

`info` Menu: Individual Utilities: `false: [coreutils] false invocation.` **Do nothing, unsuccessfully.**
