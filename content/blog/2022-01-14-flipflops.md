---
title: "Things Not To Do 101: Fucking Flip Flops"
date: 2022-01-14T17:54:32Z
tags: ["stream of consciousness", "i'm an asshole"]
categories: ["stream of consciousness"]
---

Given my proclivity for reaching an insane arbitrary level of *“not taking it anymore”* and walking out the door on a whim.. I should not go with flip flops as my **default** and **only** choice of foot covering. Feet hitting pavement with a thin layer of *“foam”* as an afterthought to comfort is surefire pain.

I do this to myself.
