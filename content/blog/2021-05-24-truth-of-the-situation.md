---
title: "Truth of the Situation"
date: 2021-05-24T14:39:40Z
categories: ["blog"]
tags: ["sobriety", "meth chronicles"]
summary: "My widget is lying."
draft: false
---

According to the widget on my phone's main screen, I should have 20 days of clean time accrued behind knee today. Quickly coming up on my first month of stringed together consecutive 24 hours periods of living and existing without meth flooding my veins.

My widget is lying. Or, to be more honest, I never turned off the count-up or uninstalled the app. 
