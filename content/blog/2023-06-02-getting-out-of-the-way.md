---
title: "Getting out of the way"
date: 2023-06-02T16:57:33Z
categories: ["blog"]
---

**A major lesson in my life** right now, that I'm still not fully comprehending, is staying out of the way and letting things happen like they should instead of how I think they should. My time here at Family Dollar is serving multiple purposes: both monetarily and therapeutically. And I keep getting anxious and find myself slipping back to old, ungrateful, habits. Staying in bed until the very last second, counting minutes while on the clock, stealing as many breaks as possible.. All of which just serves to make myself miserable and unfocused. 

Another example of me throwing a wrench in the spokes of my life happened last night while I was taking a smoke break. A homeless[^1] dude was on the side of the building where I smoke cigs, and he asked me if he could use my lighter. Next question was if I smoked weed. My immediate answer was "Yes" and next there I was taking a hit. This counters my chance to get a better paying and less stressful job. I had just applied with Via to drive for Trinity Metro's Zip-Zone.. The job, being a DOT regulated position, has stringent drug-use policies and tests to do before being hired. 

My plan had been to clear out my system for the rest of the week and weekend and go take the test on Monday or Tuesday of next week. Before last night, I hadn't smoked pot in a couple weeks and had only[^2] been using meth. Now, I have to configure and contrive some excuse as to why I can't take my drug test for another week past when I originally declared (to my mother as well as Via) to do so...

Because, after all, I am my own worst enemy.

[^1]: Assumed to be homeless. 

[^2]: Heavily.
