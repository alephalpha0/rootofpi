---
title: "Beware the Ides of March"
date: 2021-03-15T12:18:44Z
tags: ["mental state", "health"]
categories: ["blog"]
summary: "Poor Julius Caesar."
draft: false
---

Poor Julius Caesar. Stabbed by everyone he knew so no one would be to blame for his death. If only I could be so lucky. I have no one to blame for my downfall except myself. 
