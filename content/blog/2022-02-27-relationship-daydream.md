---
title: "Relationship Daydreams."
date: 2022-02-27T23:47:37
tags: ["meth chronicles", "real life"]
categories: ["blog"]
summary: "Morbid shifting of my ideal relationship."
---

**my daydream of the perfect** relationship used to be mainly focused on the idea of someone that would love nothing more than to lounge around for weeks on end, eating acid and smoking Mary J while we listened to Autechre albums and cuddled and made art and slept and introduced ourselves to Aliens. 

**that has changed, naturally.** these days, I just daydream about someone who is going to love me when I'm coming down and losing my mind. someone to pick up my pieces before whatever's happening to me picks up momentum and becomes What Has Happened Instead. 

انا احتضر 

Written under the influence of *Autechre - O=0*.
