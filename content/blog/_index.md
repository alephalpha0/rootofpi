**I have been keeping** a journal, offline and online, for most of my life. It's s tremendous help to my mental state to put down my thoughts through physical action. A generous release for my soul. 
