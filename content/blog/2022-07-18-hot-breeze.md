---
title: "Hot Breeze"
date: 2022-07-18T11:53:44Z
categories: ["stream of consciousness"]
tags: ["stream of consciousness", "summer blues"]
---

The temperature, according to my phone, is 108 degrees fahrenheit. According to a customer's dashboard in their car, it's 115. Either way: the breeze blowing burns my cheeks and hurts to be in. This summer is not even half way done, and it is already hell.

38th Birthday is in little under a month.. 
