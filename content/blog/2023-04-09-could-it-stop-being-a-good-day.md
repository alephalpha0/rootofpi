---
title: "Could it stop being a good day?"
date: 2023-04-09T21:16:23Z
categories: ["blog"]
tags: ["homelessness", "counting my blessings"]
---

**Could it stop** being a good day? I'm not used to this level. I'm not used to this level of blessed. Blessed is not a phrase I throw around lightly and easily. When someone responds to the question "how are you?" with "I'm blessed" or, even worse, "blessed and highly favored," my initial reaction is to roll my eyes.

Not because I don't believe them, but because I hold the belief that every being on this earth is blessed and highly favored.. Otherwise, they wouldn't be here on this planet. We are all blessed just by our continued existence.

But, today, I am blessed and highly aware of the fact. There can be no other option than to know that fact.

* I am homeless, still, for the past week. Yet haven't stayed a night outdoors. Yet.
* I got my job at Family Dollar back and started working the next day. I was hired yesterday (4/8).
* Lisa came by work to buy cigs. While she was leaving, she asked me if I was coming home tonight. I was scrounging to come up with a place to stay, and she saved my ass once again.
* My mother. All the various and multifaceted ways she is trying to help and not endanger herself. I need to dwell on this fact more and try and grow up more because of it.
* The payments from gigs last week have not paid out just yet. So that means that the money has not been spent and will arrive in a larger sum than piece-meal like I was planning on.
* "Bad Boss" Lana offered to open her couch to me a couple nights a week. 
* More than I can enumerate.
