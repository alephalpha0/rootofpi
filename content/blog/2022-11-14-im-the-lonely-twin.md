---
title: "I'm the lonely twin"
date: 2022-11-14T11:53:44Z
categories: ["blog", "stream of consciousness"]
tags: ["gorillaz", "song lyrics", "stream of consciousness"]
draft: false
---
> I'm the lonely twin, the left hand  
> (If I pick it up when I know that it's broken Do I put it back?)  
> I don't want this isolation  
> (Or do I head out onto the lonesome trail)  
> See the state I'm in now?  
> (And let you down?)  

[Outro]

> If I pick it up when I know that it's broken  
> Do I put it back?  
> Or do I head out onto the lonesome trail  
> And let you go?
