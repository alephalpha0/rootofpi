---
title: "The Depths"
date: 2023-06-18T14:50:14Z
---

**Usually, I don't like to write for myself** when I'm depressed. Unless I'm being angsty and bitching about some situation, I have no desire to leave a record of my depths. This isn't my usual depth tho. I'm back at weighing the pros and cons of continued existence. I hurt on many levels and there doesn't seem to be any way to soothe the wounds. 

Mom and Terry are almost finished building their house in Clifton. Estimated time of their departure is a little over a month from now. This is still being processed in my mind. My mother is leaving the city and is moving to a location that I have no means of reaching on my own. Our weekly dinners will become monthly ones. The house I grew up in will be sold and another portion of my past (also my version of normality) will be lost to the passage of time. 

My sister refuses to talk to me or acknowledge me. No phone calls or texts from me are returned. Radio silence and I'm left reeling. 

I am alone and lonely. I have no tribe, no partner, no friends. Dustin has finally gone his way as I knew he would, but sooner than expected. Zac is dead. Anyone that I gave my time to in the past decade is nowhere to be found. I have no real clue how to start friendships again with new people. 

The routine and daily grind are getting to me. I want to be doing something other than retail, but I have effectively retarded myself in regards to being able to focus and work on anything meaningful or difficult for any long length of time.

I allowed meth to take over my life and it did with it exactly what was promised. I shouldn't be surprised, and I'm not really.. Just beaten down and ready to give up the ghost.
