---
title: "My Lane"
date: 2021-03-21T09:18:49-05:00
draft: false
categories: ["blog"]
tags: ["sobriety", "mental state", "self assessment"]
summary: "Bad habit of being irritated by other's actions and trying to fix their actions to make myself happy."
---

> ..Grant me the serenity to accept the things I cannot change.

Must keep remembering to keep in my lane. Bad habit of being irritated by other's actions and trying to fix their actions to make myself happy. I can't afford to give away my happy to other people or to spend my energy on anything other than myself. Try to keep this in mind; extension of that is try to keep a state of mindfulness. It's hard, I know. But it's something to practice.
