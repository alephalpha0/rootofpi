---
title: "Another First"
date: 2023-04-07T21:00:44Z
draft: false
categories: ["blog"]
tags: ["meth chronicles", "ramblings of an asylum inmate", "homelessness"]
---

**Here's another first** in my life: I'm homeless. Literally and figuratively: homeless. I have no home, no room, no any-sort-of-shelter for myself. It's Friday night now and this happened on Monday. Really, the *happening* happened over the course of the past half year or so. I allowed myself to become bitter and sullen and nasty. I treated the staff of where I lived like shit, and didn't make any concessions towards improving myself any-more. 

On top of the general shit-head attitude I was rocking, I wasn't paying rent. This one-two wombo-combo added up to be a killer combo that took me out of the possibility to make up the financial portion of the eviction and remain in residence while doing so. The words used to convey this to me went something along the lines of: "you're nasty to the staff and they don't want you here." 

That was last Thursday morning. Friday the constable showed up to tape the eviction notice to my door. Monday morning, 09:30, the knock came and I was evicted.

There was a chance to get into the Salvation Army's program to have a place to stay and a base to get jobs from.. But I was dirty on the UA and that door slammed shut. I spent the rest of the day riding the TRE back and forth from downtowns FTW and Dallas. Eventually got off the train to get a drink at the QT back near ground-zero and took a chance on hoping a past friend would let me stay the night with her.

Now it's been a week and I'm still on her couch. I worked a gig on Tuesday and another on Wednesday. Had hoped that the Wednesday one would have paid out by now, but it's still pending manual time sheet approval and will probably hit on Monday evening. That would allow me to get a room on Monday night and possible Tuesday into Wednesday. Then by Friday the other gig will payout (and any other gigs I'm able to get over the weekend on the InstaWork app.)

And then the cycle will start over and over and over and over. I didn't want to be on my friend's couch for more than a couple days. And that time frame has zoomed on by. I don't want to burn a bridge here. Especially after showing up out of the blue and being welcomed in by her. I've been told that I can stay until the payouts, but then I also said I'd pay her some $$$ and I'd need that to get the motel room to make it till the other payout. 

**My saving grace** in all of this is Family Dollar, as weird as that all sounds. I hadn't had the courage to show my face there after PTSD took over the night of the hold-up there until a couple weeks ago or so. Since then, I've seen most of the managers on the times I frequented the store and they all made it known that I was welcome to come back. Then I heard today that Lupe, store manager, wanted to talk to me.

That will happen tomorrow afternoon. If I'm lucky, I could work two shifts right off the bat, and do the stupid thing of getting DailyPay going and immediately have money to get the room for longer, as well as for food or whatever.. 

# Fuck. Fuck. Fuck.
