---
title: "The Ghost Spoke Again Today."
date: 2023-04-23T21:46:22Z
categories: ["blog"]
---

**There is a spirit inhabiting** the store I work at. It made itself known to me the first time I worked there. It doesn't seem mean or feel threatening, it simply likes to knock shit down occasionally. It likes to make itself known. 

Today, I was checking a customer out (both literally and figuratively, the fellow was FAF[^fn1],) when a display by the door abruptly tumbled over sans physical contact. The ghost[^fn2] once again seemed to feel the urge to be known by we other occupants. It was the largest display of selfhood from the haunt I have been privileged to experience to date. I wonder who it was trying to express itself to.. What did it want to say? 

[^fn1]: Fine as fuck
[^fn2]: Or spirit, or daemon, or... Still not sure of the spirit's organizational category. 
