---
title: "Weird Pleasure"
date: 2023-06-25T23:10:04Z
categories: ["blog"]
tags: ["stream of consciousness"]
---

**I get extreme weird pleasure** out of pulling facial hairs out right by the corner of my mouth. I discovered this when I first started growing a beard. The follicles have this gunk at the end of them that lets me stick the hairs to anything I please. The feeling once I've plucked them out is divine and only serves to drive me ritual of pulling out hairs and sticking them on things. 
