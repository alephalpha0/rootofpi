---
date: 2022-09-13 12:57:07
title: The Medical Market TRE Station's New Description
categories: ["stream of consciousness"]
tags: ["self amusement", "alternate dimensions",  "stream of consciousness", "my side of the mirror"]
---
This TRE Station contains a magical split personality. In the early morning and early evening: it plays the role of middle class mass transit ingress and egress with fantastical skill and dexterity. Once the Dallas-bound professionals scurry away to their 9-5 private, decent-paying, hellish tar pits: the station Jekyllizes into the private playground of Dallas' unique blend of the mentally unstable and perma-skitzing homeless population. 

Don't be alarmed should you come across a human tooth or feces-soiled sports coat on the ground, they are merely last night's offering to the God of the Mound. They will be eaten before the next morning.

And yes. That is urine that you're smelling. But it isn't from a corporal being. 

[edan.io page linked on google maps](http://medical-station.poi.place/?ref=pv)
