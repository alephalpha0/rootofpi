---
title: "Current email sig.."
date: 2022-07-08T05:18:28Z
categories: ["stream of consciousness"]
tags: ["command line gazing", "terminal blues", "email", "banners", "stream of consciousness"]
summary: "Mors certa; vita incerta.."
---


```
░(~:/2022\:_:_)[http://alephalpha0.srht.site]░
░░█▀█░█░░░█▀▀░█▀█░█░█░█▀█░█░░░█▀█░█░█░█▀█░▄▀▄░
░░█▀█░█░░░█▀▀░█▀▀░█▀█░█▀█░█░░░█▀▀░█▀█░█▀█░█/█░
░░▀░▀░▀▀▀░▀▀▀░▀░░░▀░▀░▀░▀░▀▀▀░▀░░░▀░▀░▀░▀░░▀░░
░░░░░░░░░░░░░░MORS░CERTA░VITA░INCERTA.░░░░░░░░
```
