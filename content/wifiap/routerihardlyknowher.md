---
title: "[WiFi] Router I Hardly Know Her"
date: 2022-04-21T04:41:34Z
draft: false
categories: ["WiFi AP"]
tags: ["wifi", "very punny"]
---

![Router, I Hardly Know Her](/img/wifiap/wifi-routerihardlyknowher.png)

**Somewhere**, the Marx brothers are very very pleased with this reference.
