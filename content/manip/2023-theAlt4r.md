---
title: "[Manip] The Alt4rr of Omni BigFace."
date: 2023-04-20T20:59:52Z
categories: ["photo manipulation"]
tags: ["glitch", "Tuesday NeXT", "alternate dimensions"]
---

![The Alt4rr of Omni BigFace.](/img/manip/theAlt4r.jpg)
