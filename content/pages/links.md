---
title: "Links"
menu: "main"
---

# The highly coveted hoarde of links.

## Pages By Artists / Pages About Art
* [reset survivor - resetsurvivor.com](http://resetsurvivor.com)
* [Party In The Front - luismelo.net](https://luismelo.net)
* [jn3008 - tumblr.com](https://jn3008.tumblr.com/)
* [artcontext.org](http://artcontext.org)  
* [ello - ello.co](https://ello.co)

## People / Blogs / Digital Gardens
* [Cregox - cregox.net](https://cregox.net/)
* [glow - glow.li](https://glow.li)
* [Fionn - hotlinecafe.com](https://hotlinecafe.com)
* [Case Duckworth - acdw.net](https://acdw.net)
* [Robert Heaton - robertheaton.com](https://robertheaton.com/)
* [ben fry - benfry.com](https://benfry.com/)
* [ReAssembler](http://reassembler.blogspot.com/)

## Collectives / Co-Operatives
* [rawtextclub](http://rawtext.club)
* [thugcrowd](https://thugcrowd.com/)  
* [Suricrasia Online](https://suricrasia.online)  
* [CAT-V](https://cat-v.org)  
* [BLOCKTRONICS](https://blocktronics.org)  
* [9Front](http://9front.org) 
* [envs.net](https://envs.net)
* [spf.org](https://spf.org)
 
## Technology Esoterica
* [Soulseek](https://slsknet.org)
* [WiGLE](https://wigle.net)
* [PICO-8](https://www.lexaloffle.com/pico-8.php)
* [TempleOS](https://templeos.org) 
* [Plan 9](https://9p.io)
* [Gemini Protocol](https://gemini.circumlunar.space/)  
* [textfiles](http://textfiles.com) 
* [Orca](https://hundredrabbits.itch.io/orca)
* [FidoNet](https://fidonet.org)
* [TELNET BBS GUIDE](https://www.telnetbbsguide.com/)

## Code Related && Repositories of VCS'
* [sr.ht](https://sr.ht) / [sourcehut.org](https://sourcehut.org)
* [devdocs.io](https://devdocs.io)
* [ShellCheck](https://shellcheck.net) 
* [Processing](https://processing.org)

## Font Foundaries / Collections  
* [Arkandis Digital Foundry](http://arkandis.tuxfamily.org/)
* [The Font Bureau](https://fontbureau.typenetwork.com)
* [int10h](https://int10h.org/)

## The Scene &TM;
* [16colo.rs](https://16colo.rs/)  
* [ansilove](https://www.ansilove.org/)  
* [trueschool.se](https://www.trueschool.se/)  
* [artpacks.org](https://artpacks.org)  
* [artscene @ textfiles](http://artscene.textfiles.com)  
* [DEMOZOO](https://demozoo.org)  
* [scene.org](https://scene.org)  
* [defacto2](https://defacto2.net/)  
* [pouet.net](https://www.pouet.net/)  

## ??!?!?!!?????
* [index-of](http://index-of.es/)
* [Windows93](https://windows93.net)
* [WiFi-SM](http://unbehagen.com/wifism)
* [AOL 4.0 2020](https://aolemu.com)
* [The End of the Internet](https://hmpg.net/) 

---
  
## 88x31
[![Proud Customer of Suricrasia Online!](/img/buttons/shork.png)](https://suricrasia.online)
[![Neocities](/img/buttons/neocities.gif)](https://neocities.org)
