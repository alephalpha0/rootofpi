---
title: "Quotes"
description: "Quotes: Strings of words organized by the mouths of other human beings"
date: 2021-06-27T08:04:33Z
draft: false
menu: main
tags: ["lyrics", "word porn", "quotations", "so i says to mabel i says"]
summary: "A magpie's consumption and collection of said sayings and things left in the public record."
---

# Quotes (mainly musical)
> I hoard quotes like a magpie. Lyrics are always stuck in my head, repeating endlessly until they become a mantra. Word porn is almost always screen-shotted and saved to the external HDD.  An inexhaustive list follows.

---

## Sung Manifestations:

##### ~ Peter Gabriel
> All you do is call me, I'll be anything you need.  

##### ~ Bob Dylan
> He not busy being born is busy dying.  

> Don't get up gentlemen, I'm only passing through.  

##### ~ David Bowie
> We know Major Tom's a junkie.  

##### ~ Freddie Mercury
> I've just got to get out of this prison cell. One day I'm going to be free.  

##### ~ John Baldwin Gourley
> Without you, I don't know what I believe in. Without you, hell should be easier.  

> I just want to be evil.  

##### ~ Glass Animals
> Why don't you dance like you're sick in your mind?

##### ~ Josh Homme
> Pleasantly caving in;  I come undone.  

##### ~ James Murphy
> Everybody makes mistakes, but it seems it's always mine that keep on stinging.  

##### ~ Lorde
> Let's embrace the point of no return.  

##### ~ Jeff Mangum / Neutral Milk Hotel
> Know all your enemies. We know who our enemies are.  

---

## The Written Word:

##### ~ Hunter S. Thompson
> Buy the ticket; ride the ride.  

#####  ~ Friedrich Nietzsche
> You have always approached everything terrible trustfully. You have wanted to pet every monster.  

#####  ~ Joseph Campbell
> The privilege of a lifetime is being who you are.  

##### ~ Carl Jung
> Until you make the unconscious conscious, it will direct your life and you will call it fate.  
> ~ Carl Jung

##### Messiah's Handbook : Reminders for the Advanced Soul
> The world is your exercise-book, the pages on which you do your sums. It is not reality, although you can express reality there if you wish. You are also free to write nonsense, or lies, or to tear the pages.  

---

## Said sayings:

##### ~ Sir Thomas Beecham
> The function of music is to release us from the tyranny of conscious thought.  

##### ~ Alan Watts
> You are an aperture through which The Universe is looking at and exploring Itself subjectively.  

##### ~ Charles Bukowski
> You learn how to survive, man, by surviving: and I haven't even been paid yet!  

##### ~ LeVar Burton
> ... Literature for me is one way to continue to explore those places inside myself that I am missing. Those areas where I am deficient. ...  

##### ~ George Carlin
> "We think in language. The quality of our thoughts & ideas can only be as good as the quality of our language.”  
