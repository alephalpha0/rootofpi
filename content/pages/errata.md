---
title: "Site's Errata of ROOTOFPI.ORG"
date: 2023-02-13T10:57:36Z
draft: false
---

```
 __________ _________ _________   _______   _________   _______  
/   /_____/|    _o___)    _o___) /   O   \ /__     __\ /   O   \ 
\___\%%%%%'|___|\____\___|\____\/___/%\___\`%%|___|%%'/___/%\___\
 `BBBBBBBB' `BB' `BBB'`BB' `BBB'`BB'   `BB'    `B'    `BB'   `BB'
```
## ABOUT_ME();
I am dave k (alephalpha0). 38 and a gay texan. I am interested in the homebrew, creative, collaborative web that existed before the corporations took over the internet. I am forever searching for the perfect form of self-expression, which has forever proven itself to be purely elusive.  I am a entry-level coder, journalist, and artist. I am also a drug addict. My hope and goal is to document my struggles with reality in my blog, as well as showcase the small spark of the Divine that occasionally shows itself within.

I come in peace.

### This content is incredibly explicit at times. We recommend and encourage only those beings above their local age of consent access this site and its archives. 


## ABOUT_SITE();
Originated circa 2003, ROOTOFPI has been the digital self-curated domain of DK3's whim and fancies. The first proto-versions were rough as fuck and hosted on angelfire & geocities, and were generally centered around adopting Norns from the game Creatures. It was middle school, what the fuck else was I going to make a site about? 
  
Sometime during High School, [skizzers.org](https://skizzers.org) was kind enough to open their server to me and allow me to continue broadcasting into the nether. The tone of the site turned emo-dark and I learned to be mediocre at HTML and CSS and journaling.This initial era closed up with a couple personal domains [chibidave.net and rootofpi.org] and my graduation from high school. Then life took over and my personal site went dormant from 2004-2020.  
  
Rootofpi's hallmarks are 1) our history is steeped in handcoding and has been mostly on the fly and off the cuff, 2) self introspection, self growth, and self amusement are the only interests and motives operating behind its existence, and 3) the term rootofpi itself; inherently _nonlogical_ and _nonsensicle_.  
 
It is my home is cyberspace that has been absent for over a decade.  

I am **incredibly** satisfied and delighted to have it back in operation and broadcasting into the nether.  

This current iteration of my site is fueled by [Cream Sodas](https://en.m.wikipedia.org/wiki/Cream_soda), its source files are hacked out with the use of the [micro](https://micro-editor.github.io/) text editor, iterated over and generated with [Hugo](https://gohugo.io), and hosted using the weird demi-urge [ionos](https://ionos.com) because they're cheap and so am I.. 

This site would not be in existence if it weren't for the ability to right click on any page on the internet and `view-source` to gaze into the innards of a site. I encourage you to do so more often.


## CORE_PHILOSOPHY();
Open Source and Libre software/mentality has long been a personal fascination and default go-to of mine. The idea that information is power is one of the tenants of our current reality. Information has many forms: data streams, sound waves, written lexicons, belief systems. My belief is that information should be free for anyone regardless of situational circumstances.



## MOVEMENTS_&CAUSES();
* Since I began terrorizing the etherspace of the world wide web in 2002, I have been a staunch supporter of [no www](https://no-www.org).
  
* Privacy and ownership of one's data, especially in the reality we find ourselvesin, is of the upmost importance. Google, Microsoft, Amazon, Facebook, the list goes forever on. These corporation desire one thing only: to own you and your actions. When you do not hold access to your data, your secrets, your very day to day affairs, you allow the services that handle that data to have control over you. There are many talks, papers, essays, and movements out there that give better urgency and validity to this thought that I ever could. [The FREENET Project](https://freenetproject.org) && [IPFS](https://ipfs.io) && [Starpage.com's "Privacy, Please"](https://www.startpage.com/privacy-please/) && [EPIC : Electronic Privacy Information Center](https://www.epic.org/).  

---  
![foobar](/img/buttons/foobar.png)   ![HTTrack](/img/buttons/httbutton.gif)
# TBA

