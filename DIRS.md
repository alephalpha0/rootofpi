./								<-- rootofpi/
../								<-- SPACE!!
ascii/							<-- ASCII and/or ANSI representations of images.
assets/							<-- CSS and JS and Fonts
content/						<-- The meat and potatoes of site. Sections should be
content/blog/						self-explanatory.
content/drawings/
content/gifs/
content/manip/
content/mixtape/
content/pages/
content/photos/
content/ss/
content/tech/
content/wifiap/
site/							<-- The hugo version of rootofpi. Sym links to
									content/ are included.
site/static/css/				<-- CSS for site.
site/static/img/				<-- Images are kept here (for some reason.)
site/static/img/buttons/
site/static/img/drawings/
site/static/img/ghost-images/
site/static/img/gifs/
site/static/img/manip/
site/static/img/photos/
site/static/img/ss/
site/static/img/wifiap/
