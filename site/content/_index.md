[![rootofpi.org logo](/img/logo_transparent.png)]()

> Tout choses sont dites deja, mais comme personne n'ecoute, il faut toujours recommencer. ~ A. Gide

**so here we are.** one to one, equal odds in both favors. hello, i'm dave. i'm an addict, semi-active self-introspective [journal](/blog/) writer, self-amused [photographer](/photos/), [digital](http://alephalpha0.space/ascii) [artist](/manip/),  & [image](/gifs/) [collector](/wifiap/) [[1]](/ss/). stay a while and [listen](/mixtape/).

[![Proud Supporter and Apostle of Web 3.14](/img/buttons/web314.png)](http://web3.14159.annwfn.net/)  
![BRAK NOW](/img/buttons/braknow.gif) ![internet roadkill](/img/buttons/internet-roadkill.gif) ![Camp DIY](/img/buttons/thecamp.gif) 
  
[LISTS](http://alephalpha0.lists.sh) && [ASCII](http://alephalpha0.space/ascii) && [SHAARLI](https://shaarli.alephalpha0.space) && [RSS](https://rss.alephalpha0.space) && [BRAIN](https://brain.alephalpha0.space)

[<<](https://hotlinewebring.club/rootofpi/previous)....[hotline.webring](https://hotlinewebring.club/).....[>>](https://hotlinewebring.club/rootofpi/next)
